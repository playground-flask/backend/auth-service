#! /bin/bash

curl -X POST http://localhost:3000/lists \
	-H "Authorization:bearer 12345" \
	-H "Content-Type: application/json" \
	-d '{"name":"Groceries","items":[{"name":"milk","completed":false},{"name":"cookies","completed":false}]}'

