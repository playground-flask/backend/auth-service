#! /bin/bash

LIST_ID=$1

curl -X PUT http://localhost:3000/lists/$LIST_ID \
	-H "Authorization:bearer 12345" \
	-H "Content-Type: application/json" \
	-d '{"items":[{"name":"milk","completed":true},{"name":"cookies","completed":true}]}'


