#! /bin/bash -x

# starts the dynamo container if not already started.
docker compose up -d

TABLE_NAME=lots-of-lists
COUNT=`aws dynamodb list-tables --endpoint-url=http://localhost:8000 | grep $TABLE_NAME | wc -l | sed -e 's/[^0-9]//g'`

# creates App table if doesn't already exist
if [ "$COUNT" -eq "0" ]; then
    echo "Creating Dynamo DB Table $TABLE_NAME";
	aws dynamodb create-table --table-name $TABLE_NAME \
  		--attribute-definitions AttributeName=userId,AttributeType=S AttributeName=listId,AttributeType=S \
  		--key-schema AttributeName=userId,KeyType=HASH AttributeName=listId,KeyType=RANGE \
  		--provisioned-throughput ReadCapacityUnits=2,WriteCapacityUnits=2 \
  		--endpoint-url=http://localhost:8000
fi

sam local start-api --parameter-overrides ExecEnv=local --docker-network dynamodb-local --warm-containers EAGER


