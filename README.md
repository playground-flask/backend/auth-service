# auth-service

This project is a stub.  It's meant to be integrated with something to be the real world
such as ldap.  For this reason it will accept any password you give as valid unless:

  * It containes the word "bad" in which case it will fail authorization.
  * It containes the word "locked" in which case it will inform you you are locked out.

It is based on this article about making a flask API with SAM:

  * https://thecodinginterface.com/blog/aws-sam-serverless-rest-api-with-flask/

# How project was initialized:

  * Install AWS cli and SAM clients.
  * Set up my amazon user with enough privs to work.
  * am init --runtime python3.9 --name auth-service
    * 1 - AWS Quick Start Template.
    * 1 - Hello World Example.

# How to test project locally:

  * sam build
  * sam local start-api
  * curl http://localhost:3000/hello

